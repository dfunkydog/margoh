<?php
/**
 * The template for displaying Category Archive pages
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ): ?>
<div class="col-3"><h2 class="page-title"><?php global $cat; ?>
<?php
$category = get_the_category(); 
echo $category[0]->cat_name;
?></h2><?php echo category_description( $cat );  ?>
<p class="page-nav"><?php posts_nav_link(' | ','Previous','Next'); ?></p> 
</div>
<div id="the-posts" class="col-8 omega">
<?php while ( have_posts() ) : the_post(); ?>
	
		<article>
			<h3 class="post-title"><?php the_title(); ?></h3>
			<?php 
			$subtitle = $meta_values = get_post_meta( get_the_ID(), 'subtitle', true );;
			echo '<h5>'.$subtitle.'</h5>'; ?>
			
			<?php the_content(); ?>
		</article>
	
<?php endwhile; ?>
</div>
<?php else: ?>
<h2>No posts to display in <?php echo single_cat_title( '', false ); ?></h2>
<?php endif; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>