jQuery(document).ready(function($) {
	$(".category article").each(function(){	
	   var sharePos = $(this);
	   $(this).find('p').last().append('<span class="sharetrigger"> | share</span>');
	});

	$('.sharetrigger').click(function(){
		var ssba = $(this).parent().next();
		if(!ssba.is(":visible")){  
				ssba.fadeIn(200, function(){
					setTimeout(function() {
						ssba.fadeOut();
					}, 5000);
				});
			} else {
				ssba.fadeOut(200);
			}		   			
		});
	
});