<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file 
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
 <?php query_posts(); ?>
<?php if ( have_posts() ): ?>
	

	<div>
		<h2>The Fold</h2>
		<p>Our workstations are flexible, giving you the opportunity to take on a small space and make it your own. Giving you the comforts of a personal office without the prohebitive pricetags.</p>
		<p>Giving you the comforts of a personal office without the prohibitive pricetags.</p>
	</div>
		<?php while ( have_posts() ) : the_post(); ?>
	
		<article>
			<h3><a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
			<time datetime="<?php the_time( 'd-m-Y' ); ?>" pubdate><?php the_date(); ?></time> 
			<?php the_excerpt(); ?>
		</article>
	
<?php endwhile; ?>

<?php else: ?>
	<div id="page-info">
		<h2>No Posts to  display</h2>
		<p>The lates new on the Double Blus Brain initiative. Find out who we met, what we did and how things are going.</p>
		<?php while ( have_posts() ) : the_post(); ?>
	</div>
<?php endif; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer') ); ?>